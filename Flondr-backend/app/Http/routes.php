<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the Closure to execute when that URI is requested.
|
*/

Route::group(['namespace' => 'Admin'], function() {

	Route::get('/dashboard', array('as' => 'dashboard', 'uses' => 'AdminController@dashboard'));

	Route::group(['prefix' => 'database'], function() {

		Route::get('/activity', array('as' => 'activityTable', 'uses' => 'AdminController@showActivityData'));

	});

});

Route::group(['namespace' => 'Login'], function() {

	Route::get('/', array('as' => 'loginPage', 'uses' => 'LoginController@index'));

});

//Route::get('/', array('as' => 'dashboard', 'uses' => 'LoginController@welcome'));

