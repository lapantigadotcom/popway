<?php namespace App\Http\Controllers\Login;

use App\Http\Controllers\Controller;

class LoginController extends Controller {

	/*
	|--------------------------------------------------------------------------
	| Login Controller
	|--------------------------------------------------------------------------
	|
	| This controller renders your application's "Login Page" for administrator
	|
	*/

	/**
	 * Create a new controller instance.
	 *
	 * @return void
	 */
	public function __construct()
	{
		$this->middleware('guest');
	}

	public function index()
	{
		return view('login');
	}

}