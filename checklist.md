# Project Checklist

List pada last commit 63d8a11250e67fc4ae0b14f8e56b566c8034ad63 tanggal 03/06/2016 pukul 06:25 AM.

## Flondr Android app

### Design
* [x] Splash Screen
* [x] Login Screen
* [x] Signup Screen
* [x] Outlet page
* [x] Outlet Gallery
* [x] Search Places
* [x] Nearby Me
* [ ] Bookmark
* [ ] Upload Photos
* [x] Profile Page
* [x] More Info on Profile
* [x] Find Promo

Apabila ada yang belum di list maka design belum ada.


### Activity Controller
* [x] Splash Screen
* [x] Login Screen
  * User masih dalam bentuk test user. Apabila connectivity handler sudah selesai, controller ini bisa melakukan login normal.
* [ ] Sign Up
  * Menunggu backend parsial jadi fungsi masih kosong
* [ ] Write Review
  * Menunggu database jadi dan ada handler di backend untuk menjalankan ini.
* [ ] Upload Photo
* [ ] Find Promo
* [ ] Nearby Me
  * Controller untuk mendapatkan koordinat masih buggy
* [ ] Search Places
* [ ] Bookmark
* [ ] Outlet Page
  * Maps masih menggunakan client side dengan bantuan maps API. Pada saat production release akan diganti menjadi server side.
* [ ] Outlet Gallery


### Functionality Handler

* [x] Session Handler
* [ ] Temporary File Handler
  * In progress
* [ ] Connection Handler
  * In progress
* [ ] Pinpoint Handler
  * Buggy
* [x] Activity Handler
  * Functioning properly. Need optimization.
* [x] FontAwesome Handler
