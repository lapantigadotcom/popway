-- MySQL Workbench Forward Engineering

SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0;
SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0;
SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='TRADITIONAL,ALLOW_INVALID_DATES';

-- -----------------------------------------------------
-- Schema mydb
-- -----------------------------------------------------
-- -----------------------------------------------------
-- Schema flondr
-- -----------------------------------------------------

-- -----------------------------------------------------
-- Schema flondr
-- -----------------------------------------------------
CREATE SCHEMA IF NOT EXISTS `flondr` DEFAULT CHARACTER SET utf8 ;
USE `flondr` ;

-- -----------------------------------------------------
-- Table `flondr`.`user`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `flondr`.`user` (
  `id` INT(11) UNSIGNED NOT NULL,
  `email` VARCHAR(255) NOT NULL,
  `password` VARCHAR(255) NOT NULL,
  `activated` TINYINT(1) NOT NULL DEFAULT 0,
  `activation_code` VARCHAR(255) NULL,
  `activated_at` TIMESTAMP NULL DEFAULT NULL,
  `last_login` TIMESTAMP NULL DEFAULT NULL,
  `persist_code` VARCHAR(255) NULL DEFAULT NULL,
  `reset_password_code` VARCHAR(255) NULL DEFAULT NULL,
  `first_name` VARCHAR(255) NULL DEFAULT NULL,
  `last_name` VARCHAR(255) NULL DEFAULT NULL,
  `username` VARCHAR(255) NULL DEFAULT NULL,
  `address` TEXT(255) NULL,
  `phone_number` TEXT(255) NULL,
  `picture` BLOB NULL,
  `created_at` TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`),
  UNIQUE INDEX `email_UNIQUE` (`email` ASC),
  INDEX `user_activation_code_index` (`activation_code` ASC),
  INDEX `user_reset_password_code_index` (`reset_password_code` ASC))
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `flondr`.`category`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `flondr`.`category` (
  `id` INT(11) UNSIGNED NOT NULL,
  `name` VARCHAR(255) NOT NULL,
  `active` TINYINT(1) NOT NULL DEFAULT 0,
  `created_at` TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`))
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `flondr`.`subcategory`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `flondr`.`subcategory` (
  `id` INT(11) UNSIGNED NOT NULL,
  `category_id` INT(11) UNSIGNED NOT NULL,
  `name` VARCHAR(255) NOT NULL,
  `active` TINYINT(1) NULL DEFAULT 0,
  `created_at` TIMESTAMP NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` TIMESTAMP NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`),
  INDEX `fk_subcategory_category1_idx` (`category_id` ASC),
  CONSTRAINT `fk_subcategory_category1`
    FOREIGN KEY (`category_id`)
    REFERENCES `flondr`.`category` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `flondr`.`country`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `flondr`.`country` (
  `id` INT(11) UNSIGNED NOT NULL,
  `name` VARCHAR(255) NOT NULL,
  `created_at` TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`))
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `flondr`.`state`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `flondr`.`state` (
  `id` INT(11) UNSIGNED NOT NULL,
  `country_id` INT(11) UNSIGNED NOT NULL,
  `name` VARCHAR(255) NOT NULL,
  `created_at` TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`),
  INDEX `fk_state_country1_idx` (`country_id` ASC),
  CONSTRAINT `fk_state_country1`
    FOREIGN KEY (`country_id`)
    REFERENCES `flondr`.`country` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `flondr`.`city`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `flondr`.`city` (
  `id` INT UNSIGNED NOT NULL,
  `country_id` INT(11) UNSIGNED NOT NULL,
  `state_id` INT(11) UNSIGNED NOT NULL,
  `name` VARCHAR(255) NOT NULL,
  `created_at` TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`),
  INDEX `fk_city_state1_idx` (`state_id` ASC),
  INDEX `fk_city_country1_idx` (`country_id` ASC),
  CONSTRAINT `fk_city_state1`
    FOREIGN KEY (`state_id`)
    REFERENCES `flondr`.`state` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_city_country1`
    FOREIGN KEY (`country_id`)
    REFERENCES `flondr`.`country` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `flondr`.`outlet`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `flondr`.`outlet` (
  `id` INT(11) UNSIGNED NOT NULL,
  `outlet_id` INT(11) UNSIGNED NOT NULL,
  `category_id` INT(11) UNSIGNED NOT NULL,
  `subcategory_id` INT(11) UNSIGNED NOT NULL,
  `country_id` INT(11) UNSIGNED NOT NULL,
  `state_id` INT(11) UNSIGNED NOT NULL,
  `city_id` INT(11) UNSIGNED NOT NULL,
  `name` VARCHAR(255) NOT NULL,
  `address` VARCHAR(255) NOT NULL,
  `zipcode` INT(11) NOT NULL,
  `latitude` VARCHAR(255) NOT NULL,
  `longitude` VARCHAR(255) NOT NULL,
  `phone` VARCHAR(255) NOT NULL,
  `premium` TINYINT(1) NOT NULL DEFAULT 0,
  `rating` FLOAT NOT NULL DEFAULT 0,
  `outlet_hours` VARCHAR(255) NOT NULL,
  `outlet_day` VARCHAR(255) NOT NULL,
  `outlet_status` VARCHAR(255) NOT NULL DEFAULT 'CLOSE NOW',
  `more_info` TEXT(255) NULL DEFAULT NULL,
  `facebook` VARCHAR(255) NULL DEFAULT NULL,
  `instagram` VARCHAR(255) NULL DEFAULT NULL,
  `twitter` VARCHAR(255) NULL DEFAULT NULL,
  `blackberry` VARCHAR(255) NULL DEFAULT NULL,
  `whatsapp` VARCHAR(255) NULL DEFAULT NULL,
  `created_at` TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`),
  UNIQUE INDEX `outlet_id_UNIQUE` (`outlet_id` ASC),
  INDEX `fk_outlet_category1_idx` (`category_id` ASC),
  INDEX `fk_outlet_subcategory1_idx` (`subcategory_id` ASC),
  INDEX `fk_outlet_country1_idx` (`country_id` ASC),
  INDEX `fk_outlet_city1_idx` (`city_id` ASC),
  INDEX `fk_outlet_state1_idx` (`state_id` ASC),
  CONSTRAINT `fk_outlet_category1`
    FOREIGN KEY (`category_id`)
    REFERENCES `flondr`.`category` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_outlet_subcategory1`
    FOREIGN KEY (`subcategory_id`)
    REFERENCES `flondr`.`subcategory` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_outlet_country1`
    FOREIGN KEY (`country_id`)
    REFERENCES `flondr`.`country` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_outlet_city1`
    FOREIGN KEY (`city_id`)
    REFERENCES `flondr`.`city` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_outlet_state1`
    FOREIGN KEY (`state_id`)
    REFERENCES `flondr`.`state` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `flondr`.`activity`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `flondr`.`activity` (
  `id` INT(11) UNSIGNED NOT NULL,
  `user_id` INT(11) UNSIGNED NOT NULL,
  `outlet_id` INT(11) UNSIGNED NOT NULL,
  `action` ENUM('add_photo', 'add_review', 'update_review') NOT NULL,
  `created_at` TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`),
  INDEX `fk_activity_outlet1_idx` (`outlet_id` ASC),
  INDEX `fk_activity_user1_idx` (`user_id` ASC),
  CONSTRAINT `fk_activity_outlet1`
    FOREIGN KEY (`outlet_id`)
    REFERENCES `flondr`.`outlet` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_activity_user1`
    FOREIGN KEY (`user_id`)
    REFERENCES `flondr`.`user` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `flondr`.`promotion`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `flondr`.`promotion` (
  `id` INT(11) UNSIGNED NOT NULL,
  `outlet_id` INT(11) UNSIGNED NOT NULL,
  `name` VARCHAR(255) NOT NULL,
  `description` TEXT(255) NULL DEFAULT NULL,
  `created_at` TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`),
  INDEX `fk_promotion_outlet_idx` (`outlet_id` ASC),
  CONSTRAINT `fk_promotion_outlet`
    FOREIGN KEY (`outlet_id`)
    REFERENCES `flondr`.`outlet` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `flondr`.`review`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `flondr`.`review` (
  `id` INT(11) UNSIGNED NOT NULL,
  `user_id` INT(11) UNSIGNED NOT NULL,
  `outlet_id` INT(11) UNSIGNED NOT NULL,
  `review` TEXT(255) NOT NULL,
  `rating` FLOAT NOT NULL,
  `created_at` TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`),
  INDEX `fk_review_outlet1_idx` (`outlet_id` ASC),
  INDEX `fk_review_user1_idx` (`user_id` ASC),
  CONSTRAINT `fk_review_outlet1`
    FOREIGN KEY (`outlet_id`)
    REFERENCES `flondr`.`outlet` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_review_user1`
    FOREIGN KEY (`user_id`)
    REFERENCES `flondr`.`user` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `flondr`.`bookmark`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `flondr`.`bookmark` (
  `id` INT(11) UNSIGNED NOT NULL,
  `user_id` INT(11) UNSIGNED NOT NULL,
  `outlet_id` INT(11) UNSIGNED NOT NULL,
  `created_at` TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`, `user_id`, `outlet_id`),
  INDEX `fk_bookmark_user1_idx` (`user_id` ASC),
  INDEX `fk_bookmark_outlet1_idx` (`outlet_id` ASC),
  CONSTRAINT `fk_bookmark_user1`
    FOREIGN KEY (`user_id`)
    REFERENCES `flondr`.`user` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_bookmark_outlet1`
    FOREIGN KEY (`outlet_id`)
    REFERENCES `flondr`.`outlet` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `flondr`.`outlet_photos`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `flondr`.`outlet_photos` (
  `id` INT(11) UNSIGNED NOT NULL,
  `outlet_id` INT(11) UNSIGNED NOT NULL,
  `title` VARCHAR(255) NOT NULL,
  `picture` BLOB NOT NULL,
  `description` TEXT(255) NULL DEFAULT NULL,
  `uploaded_by` INT(11) UNSIGNED NOT NULL,
  `created_at` TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`),
  INDEX `fk_outlet_photos_outlet1_idx` (`outlet_id` ASC),
  CONSTRAINT `fk_outlet_photos_outlet1`
    FOREIGN KEY (`outlet_id`)
    REFERENCES `flondr`.`outlet` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `flondr`.`user_has_promotion`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `flondr`.`user_has_promotion` (
  `id` INT(11) UNSIGNED NOT NULL,
  `user_id` INT(11) UNSIGNED NOT NULL,
  `promotion_id` INT(11) UNSIGNED NOT NULL,
  `created_at` TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`, `user_id`, `promotion_id`),
  INDEX `fk_user_has_promotion_promotion1_idx` (`promotion_id` ASC),
  INDEX `fk_user_has_promotion_user1_idx` (`user_id` ASC),
  CONSTRAINT `fk_user_has_promotion_user1`
    FOREIGN KEY (`user_id`)
    REFERENCES `flondr`.`user` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_user_has_promotion_promotion1`
    FOREIGN KEY (`promotion_id`)
    REFERENCES `flondr`.`promotion` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;


SET SQL_MODE=@OLD_SQL_MODE;
SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS;
SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS;
