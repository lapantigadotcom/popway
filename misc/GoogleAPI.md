# Google API key for Flondr app

Key type | Value | Note
-------- |  -------- | ---------
Browser key | AIzaSyBwvMmh7HwCrPp5y33RBY8LjRPnGWF42ZE | Experimental key for generating static maps on Browser
Android Key | AIzaSyDFj-4DgP8TYkrkBCUnTMwwlzfZjGMJEzw | Android API key for accessing maps. This key is registered with debug certificate with SHA1 fingerprint: C4:78:90:5F:14:B4:A8:7C:57:83:89:2B:48:F0:E5:10:EE:73:FB:99
Server key | AIzaSyBup0OwY9E7yUrDLXSgJgJSqIRNPAqJ3V0 | Server API key for experimental function
