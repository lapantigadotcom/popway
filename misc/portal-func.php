<?php
  class RequestHandler
  {
      public static $MIN_API_VER = 10;
      public static $REJECT_MESSAGE = "Sorry, you don't have privilege to access this file";
      private static $user = array(array('username' => 'Yoga', 'pasword' => '123456'), array('username' => 'test', 'password' => 'test'), array('username' => 'beta', 'password' => 'nopassword'));
      private static $command = array('auth' => 1 );

      public function CheckApi($post)
      {
          if(!is_numeric($post["api_ver"])){
            echo "API unknown.\r\n";
            return false;
          }
          elseif ((int)$post["api_ver"]<$MIN_API_VER) {
            echo "This level of API is not supported.\r\n";
            return false;
          }
          else{
            return true;
          }
      }

      public function DoRequest($post){
        $ret = this->CheckCommandType($post);
        this->CommandBridge($post, $ret);
      }

      private function CheckCommandType($post)
      {
        // 1. Auth
        $res = 0;
        foreach (this->$command as $key => $val) {
          if($key == $post["action"]) $res = $val;
        }
        return $res;
      }

      private function CommandBridge($post, $command)
      {
        switch ($command) {
          case 1:
            Auth($post);
            break;

          default:
            echo "Command unrecognized.\r\n";
            return false;
            break;
        }
      }

      private function Auth($post){
        var_dump($user);
      }
  };
