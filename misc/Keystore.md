# flondr.keystore Details

## Description
this file is a keystore for storing certificate and private key required for flondr-android app

## Technical Details
- Keystore Password: flondr

## List of key in keystore
### places.connecting.flondr.debug
- Password: flondr
- SHA1 fingerprint: C4:78:90:5F:14:B4:A8:7C:57:83:89:2B:48:F0:E5:10:EE:73:FB:99
