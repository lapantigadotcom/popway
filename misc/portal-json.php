<?php

/**
* JSON Format:
* action  : This string for determining action taken by handler
* api-ver : This string for telling the handler which version flondr-android is running
*
*/

class RequestHandler{
    /**
    * This class is for handling request from Flondr android app.
    */

    private $user = array(array('username' => 'Yoga', 'password' => '123456'), array('username' => 'test', 'password' => 'test'), array('username' => 'beta', 'password' => 'nopassword'));

    public static function CheckHeaderValid($post){
      if($post['app'] == "flondr") return true;
      else return false;
    }

    public function Handle($json){
        $req = json_decode($json);
        if($req==NULL) return false;
        switch ($req->action) {
          case 'auth':
            echo "Auth selected.\r\n";
            return $this->Auth($req);
            break;

          default:
            $string = array("info" => "Bad action", "errno" => 0);
            echo json_encode($string);
            return false;
            break;
        }
    }

    private function Auth($obj){
      if(isset($obj->username) && isset($obj->password)){
        $carry = 0;
        foreach ($this->user as $array) {
          if($array['username'] == $obj->username){
            if($array['password'] == $obj->password){
              $string = array("info" => "User $obj->username successfully logged on", "username" => "$obj->username", "session-id" => bin2hex(openssl_random_pseudo_bytes(20)));
              echo json_encode($string);
              $carry = 1;
              return true;
            }
          }
        }
        if($carry == 0){
          $string = array("info" => "Username or password didn't match", "errno" => 1);
          echo json_encode($string);
          return true;
        }
      } else {
        $string = array("info" => "Bad auth action", "errno" => 0);
        echo json_encode($string);
        return false;
      }
    }
};
?>
<?php
$handler = new RequestHandler();
$ret = RequestHandler::CheckHeaderValid($_POST);
var_dump($ret);
// var_dump($handler);
if($ret){
  $err = $handler->Handle($_POST['payload']);
  var_dump($err);
}
?>
