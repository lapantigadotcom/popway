package places.connecting.flondr;

import android.graphics.Typeface;
import android.os.Bundle;
import android.support.design.widget.NavigationView;
import android.support.v4.content.ContextCompat;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.widget.TextView;

public class Flondr_OutletPhotosGallery extends FlondrNavigationHandler {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentViewWrapper(R.layout.activity_outlet_gallery);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);


/*
        NavigationView navigationView = (NavigationView) findViewById(R.id.navView);
        navigationView.setCheckedItem(R.id.navReview);
*/

        Typeface iconFont = FontManager.getTypeface(getApplicationContext(), FontManager.FONTAWESOME);
        FontManager.markAsIconContainer(findViewById(R.id.menuBottomContainer), iconFont);

        // For debugging purpose
        Log.i("Activity Process", "Entering intent: " + this.getLocalClassName());
    }

    @Override
    public void onBackPressed() {
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawerLayout);
        // Close navigation menu if existed
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);

            // For debugging purpose
            Log.i("Navigation Menu", "From " + this.getLocalClassName() + ": Back pressed");

        } else {
            this.finish();
        }
    }
}