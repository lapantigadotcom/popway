package places.connecting.flondr;

import android.graphics.Typeface;
import android.os.Bundle;
import android.support.design.widget.NavigationView;
import android.support.v4.content.ContextCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.widget.TextView;
import android.widget.Button;

public class Flondr_UserProfile extends FlondrNavigationHandler {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentViewWrapper(R.layout.activity_user_profile);
//        this.setUsernameForMenu();
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);



/*
        NavigationView navigationView = (NavigationView) findViewById(R.id.navView);
        navigationView.setCheckedItem(R.id.navNearby);
*/

        Typeface iconFont = FontManager.getTypeface(getApplicationContext(), FontManager.FONTAWESOME);
        FontManager.markAsIconContainer(findViewById(R.id.menuBottomContainer), iconFont);

        TextView iconSetting = (TextView) findViewById(R.id.bottomSettingButton);
        iconSetting.setTextColor(ContextCompat.getColor(this, R.color.yellow));

        Button moreInfoButton = (Button) findViewById(R.id.userMoreInfo);
        moreInfoButton.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                // get your outer relative layout
                DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawerLayout);

                // inflate content layout and add it to the relative layout as second child
                // add as second child, therefore pass index 1 (0,1,...)

                View popup = getLayoutInflater().inflate(R.layout.part_popup_info, null);
                drawer.addView(popup, 1);
            }

        });

        // For debugging purpose
        Log.i("Activity Process", "Entering intent: " + this.getLocalClassName());
    }

    public void exitFromPopupUserMoreInfo(View v) {
        // get your outer relative layout
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawerLayout);

        drawer.removeView(drawer.getChildAt(1));
    }

}