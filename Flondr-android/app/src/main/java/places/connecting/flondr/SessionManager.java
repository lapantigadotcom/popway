package places.connecting.flondr;

import android.app.Activity;
import android.content.Context;
import android.content.SharedPreferences;
import android.support.v7.app.AppCompatActivity;

/**
 * Created by Ardhinata Juari (ardhinata.juari@gmail.com) for Flondr-android on 06/03/16.
 */

/*
Class Session Manager
Kelas ini digunakan sebagai wrapper untuk mengambil persistent value dari aplikasi ini.
Kelas ini tidak perlu di deklarasikan, karena kelas ini merupakan "Singleton".
 */

public class SessionManager {
    private static SharedPreferences sharedPreferences;
    private static SharedPreferences.Editor editor;
    private static SessionManager sessionManager = new SessionManager();
    public static final String PREF_USER = "username";
    public static final String PREF_SESSION_ID = "session_id";
    public static final String NULL_SESSION_STRING = "!@NO_VALUE_!";
    public static final int NULL_SESSION_INT = 0;

    private SessionManager() {
    }

    public static SessionManager session() {
        return sessionManager;
    }

    public void initSession(Activity activity) {
        sharedPreferences = activity.getPreferences(Context.MODE_PRIVATE);
        editor = sharedPreferences.edit();
    }

    public String getUserSession(Activity activity) {
        initSession(activity);
        return this.sharedPreferences.getString(SessionManager.PREF_USER, SessionManager.NULL_SESSION_STRING);
    }

    public void SetUserSession(Activity activity, String username) {
        initSession(activity);
        editor.putString(SessionManager.PREF_USER, username);
        editor.commit();
    }

    public String getSessionId(Activity activity) {
        initSession(activity);
        return sharedPreferences.getString(SessionManager.PREF_SESSION_ID, SessionManager.NULL_SESSION_STRING);
    }

    public void setSessionId(Activity activity, String sessionId) {
        initSession(activity);
        editor.putString(SessionManager.PREF_SESSION_ID, sessionId);
        editor.commit();
    }

    public void clearSession(Activity activity) {
        initSession(activity);
        editor.clear();
        editor.commit();
    }

    public boolean anyActiveSession(Activity activity) {
        initSession(activity);
        if (this.getUserSession(activity) == SessionManager.NULL_SESSION_STRING) return false;
        else return true;
    }
}
