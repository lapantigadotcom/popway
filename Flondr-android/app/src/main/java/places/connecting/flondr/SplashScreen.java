package places.connecting.flondr;


import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.util.Log;


public class SplashScreen extends Activity {
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        // TODO Auto-generated method stub
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splash_screen);
        final SharedPreferences sharedPreferences = getPreferences(Context.MODE_PRIVATE);


        // For debugging purpose
        Log.i("Activity Process", "Entering intent: " + this.getLocalClassName());

        Thread timerThread = new Thread() {
            public void run() {
                try {
                    sleep(500);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                } finally {
                    Intent intent;
                    if (sharedPreferences.getString(SessionManager.PREF_USER, SessionManager.NULL_SESSION_STRING) == SessionManager.NULL_SESSION_STRING) {
                        intent = new Intent(SplashScreen.this, Flondr_Main.class);
                    } else intent = new Intent(SplashScreen.this, Flondr_Nearby.class);
                    startActivity(intent);
                }
            }
        };
        timerThread.start();
    }

    @Override
    protected void onPause() {
        // TODO Auto-generated method stub
        super.onPause();
        finish();
    }
}
