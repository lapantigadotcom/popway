package places.connecting.flondr;

import android.Manifest;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Typeface;
import android.net.Uri;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.NavigationView;
import android.support.v4.app.ActivityCompat;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.location.Location;
import android.location.LocationManager;
import android.location.Criteria;
import android.location.LocationListener;
import android.content.Context;

import com.google.android.gms.appindexing.Action;
import com.google.android.gms.appindexing.AppIndex;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.MapView;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;

import java.util.ArrayList;

public class Flondr_OutletPage extends FlondrNavigationHandler {

    // Map Code First Version
    private static final LatLng ONE = new LatLng(32.882216, -117.222028);
    private static final LatLng TWO = new LatLng(32.872000, -117.232004);
    private static final LatLng THREE = new LatLng(32.880252, -117.233034);
    private static final LatLng FOUR = new LatLng(32.885200, -117.226003);
    private static final int POINTS = 4;
    //*/

    // Example map code
    private GoogleMap mMap;
    private MapView mapView;

    private LocationManager locationManager;
    private String locationProvider;

    // Acquire a reference to the system Location Manager
    //private LocationManager locationManager;
    //private String locationProvider;
    /**
     * ATTENTION: This was auto-generated to implement the App Indexing API.
     * See https://g.co/AppIndexing/AndroidStudio for more information.
     */

    private ArrayList<LatLng> coords = new ArrayList<LatLng>();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentViewWrapper(R.layout.activity_outlet_page);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

/*
        NavigationView navigationView = (NavigationView) findViewById(R.id.navView);
        navigationView.setCheckedItem(R.id.navNearby);
*/

        Typeface iconFont = FontManager.getTypeface(getApplicationContext(), FontManager.FONTAWESOME);
        FontManager.markAsIconContainer(findViewById(R.id.menuBottomContainer), iconFont);
        FontManager.markAsIconContainer(findViewById(R.id.outletInfoContainer), iconFont);
        FontManager.markAsIconContainer(findViewById(R.id.outletGalleryContainer), iconFont);

        // Map Code first version
        // Gets the MapView from the XML layout and creates it

        locationManager = (LocationManager) this.getSystemService(Context.LOCATION_SERVICE);
        locationProvider = LocationManager.GPS_PROVIDER;

        mapView = (MapView) findViewById(R.id.mapView);
        mapView.onCreate(savedInstanceState);

        mapView.getMapAsync(new OnMapReadyCallback() {
            @Override
            public void onMapReady(GoogleMap googleMap) {
                Log.i("Google Map", "lol");
                setUpMap(googleMap);
            }
        });

        // For debugging purpose
        Log.i("Activity Process", "Entering intent: " + this.getLocalClassName());

    }

    @Override
    public void onBackPressed() {
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawerLayout);
        // Close navigation menu if existed
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);

            // For debugging purpose
            Log.i("Navigation Menu", "From " + this.getLocalClassName() + ": Back pressed");

        } else {
            this.finish();
        }
    }

    public void fabFunction(View v) {

        FloatingActionButton fabMain = (FloatingActionButton) findViewById(R.id.fabSpan);

        LinearLayout layFab1 = (LinearLayout) findViewById(R.id.layoutFabWriteReview);
        RelativeLayout layFab2 = (RelativeLayout) findViewById(R.id.layoutFabBookmark);
        RelativeLayout layFab3 = (RelativeLayout) findViewById(R.id.layoutFabPhotos);
        if (layFab1.getVisibility() == v.GONE) {
            fabMain.setImageResource(android.R.drawable.ic_menu_close_clear_cancel);
            layFab1.setVisibility(v.VISIBLE);
            layFab2.setVisibility(v.VISIBLE);
            layFab3.setVisibility(v.VISIBLE);
        } else {
            fabMain.setImageResource(android.R.drawable.ic_input_add);
            layFab1.setVisibility(v.GONE);
            layFab2.setVisibility(v.GONE);
            layFab3.setVisibility(v.GONE);
        }
    }

    public void fabBookmark(View v) {

    }

    public void fabWriteReview(View v) {
        Intent intent = new Intent(this, Flondr_WriteReview.class);
        startActivity(intent);
    }

    public void fabPhotos(View v) {

    }

    public void goToPhotosGallery(View v) {
        Intent intent = new Intent(this, Flondr_OutletPhotosGallery.class);
        startActivity(intent);
    }

    // Example Map Code Function
    // Map Function

    // Map Code First version
    @Override
    protected void onDestroy() {
        super.onDestroy();
        mapView.onDestroy();
    }

    @Override
    protected void onResume() {
        super.onResume();
        mapView.onResume();
    }

    @Override
    protected void onPause() {
        super.onPause();
        mapView.onPause();
    }

    @Override
    public void onLowMemory() {
        super.onLowMemory();
        mapView.onLowMemory();
    }

    @Override
    protected void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        mapView.onSaveInstanceState(outState);
    }

    //
    private void setUpMap(GoogleMap map) {

        mMap = map;

        if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            // TODO: Consider calling
            //    ActivityCompat#requestPermissions
            // here to request the missing permissions, and then overriding
            //   public void onRequestPermissionsResult(int requestCode, String[] permissions,
            //                                          int[] grantResults)
            // to handle the case where the user grants the permission. See the documentation
            // for ActivityCompat#requestPermissions for more details.
            return;
        }

        Location location = locationManager.getLastKnownLocation(locationProvider);
        double lat = location.getLatitude();
        double lng = location.getLongitude();
        LatLng coordinate = new LatLng(lat, lng);

        mMap.addMarker(new MarkerOptions()
                .position(coordinate)
                .icon(BitmapDescriptorFactory.defaultMarker(BitmapDescriptorFactory.HUE_RED)));

        mMap.moveCamera(CameraUpdateFactory.newLatLng(coordinate));

        // For example purpose
        /*
        mMap = map;
        coords.add(ONE);
        coords.add(TWO);
        coords.add(THREE);
        coords.add(FOUR);

        for (int i = 0; i < POINTS; i++) {
            mMap.addMarker(new MarkerOptions()
                    .position(coords.get(i))
                    .icon(BitmapDescriptorFactory.defaultMarker(BitmapDescriptorFactory.HUE_RED)));
        }

        mMap.moveCamera(CameraUpdateFactory.newLatLng(ONE));
        */
    }
}

