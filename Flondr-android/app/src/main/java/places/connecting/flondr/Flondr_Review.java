package places.connecting.flondr;

import android.graphics.Typeface;
import android.os.Bundle;
import android.support.design.widget.NavigationView;
import android.support.v4.content.ContextCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.widget.TextView;

public class Flondr_Review extends FlondrNavigationHandler {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentViewWrapper(R.layout.activity_review);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

/*
        NavigationView navigationView = (NavigationView) findViewById(R.id.navView);
        navigationView.setCheckedItem(R.id.navReview);
*/

        Typeface iconFont = FontManager.getTypeface(getApplicationContext(), FontManager.FONTAWESOME);
        FontManager.markAsIconContainer(findViewById(R.id.menuBottomContainer), iconFont);

        // For debugging purpose
        Log.i("Activity Process", "Entering intent: " + this.getLocalClassName());
    }
}