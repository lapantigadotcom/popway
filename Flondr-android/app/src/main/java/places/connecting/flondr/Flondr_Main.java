package places.connecting.flondr;

import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.NavigationView;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.widget.Toolbar;
import android.text.Layout;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

public class Flondr_Main extends FlondrNavigationHandler
        implements View.OnClickListener {

    // Class field
    EditText editTextUsername, editTextPassword;
    Button buttonLogin;
    TextView errorMessage;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentViewWrapper(R.layout.activity_flondr__main);

        NavigationView navigationView = (NavigationView) findViewById(R.id.navView);
        Menu menu = navigationView.getMenu();
        menu.findItem(R.id.navSignMenu).setTitle(R.string.navigation_signup);

        editTextUsername = (EditText) findViewById(R.id.username);
        editTextPassword = (EditText) findViewById(R.id.password);
        buttonLogin = (Button) findViewById(R.id.loginButton);

        // For debugging purpose
        Log.i("Activity Process", "Entering intent: " + this.getLocalClassName());

        buttonLogin.setOnClickListener(this);
    }

    // View.OnClickListerner interface override override
    @Override
    public void onClick(View v) {
        String username = editTextUsername.getText().toString();
        String password = editTextPassword.getText().toString();
        Intent intent;


        if (username.equals("Yoga") && password.equals("123456")) {

            try {
                SessionManager.session().SetUserSession(this, "Yoga");
                intent = new Intent(this, Flondr_Nearby.class);
                startActivity(intent);
                Toast.makeText(Flondr_Main.this, "logged on successfully", Toast.LENGTH_SHORT).show();
                this.finish();
            } catch (Exception e) {
                String s = e.getMessage();
                Toast.makeText(Flondr_Main.this, s, Toast.LENGTH_SHORT).show();
            }
        } else {
            Toast.makeText(Flondr_Main.this, "Wrong username or password", Toast.LENGTH_SHORT).show();
        }

    }

    public boolean onNavigationItemSelected(MenuItem item) {
        // Handle navigation view item clicks here.
        int id = item.getItemId();

        if (id == R.id.navSignMenu) {
            Intent intent;
            if (!SessionManager.session().anyActiveSession(this)) intent = new Intent(this, Flondr_Signup.class);
            else {
                SessionManager.session().clearSession(this);
                intent = new Intent(this, Flondr_Main.class);
            }
            intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
            startActivity(intent);
        } else {
            Toast.makeText(this, "You must sign in to use this menu", Toast.LENGTH_SHORT).show();
            item.setChecked(false);
        }

        // For debugging purpose
        Log.i("Navigation Menu", "From " + this.getLocalClassName() + ": Selecting " + item.toString());

        item.setChecked(false);
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawerLayout);
        drawer.closeDrawer(GravityCompat.START);

        return true;
    }

}

