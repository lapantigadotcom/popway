package places.connecting.flondr;

import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.NavigationView;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.Toast;

public class Flondr_Signup extends FlondrNavigationHandler {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentViewWrapper(R.layout.activity_signup);

        NavigationView navigationView = (NavigationView) findViewById(R.id.navView);
        Menu menu = navigationView.getMenu();
        menu.findItem(R.id.navSignMenu).setTitle("Sign In");
        navigationView.setCheckedItem(R.id.navSignMenu);

        // For debugging purpose
        Log.i("Activity Process", "Entering intent: " + this.getLocalClassName());
    }

    public boolean onNavigationItemSelected(MenuItem item) {
        // Handle navigation view item clicks here.
        int id = item.getItemId();

        if (id == R.id.navSignMenu) {
            Intent intent;
            if (!SessionManager.session().anyActiveSession(this)) intent = new Intent(this, Flondr_Main.class);
            else {
                SessionManager.session().clearSession(this);
                intent = new Intent(this, Flondr_Main.class);
            }
            intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
            startActivity(intent);
        } else {
            Toast.makeText(this, "You must sign in to use this menu", Toast.LENGTH_SHORT).show();
            item.setChecked(false);
        }

        // For debugging purpose
        Log.i("Navigation Menu", "From " + this.getLocalClassName() + ": Selecting " + item.toString());

        item.setChecked(false);
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawerLayout);
        drawer.closeDrawer(GravityCompat.START);

        return true;
    }
}