package places.connecting.flondr;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.os.Handler;
import android.support.design.widget.NavigationView;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.SearchView;
import android.widget.TextView;
import android.widget.Toast;

import org.w3c.dom.Text;

/**
 * Created by Ardhinata Juari (ardhinata.juari@gmail.com) for Flondr-android on 29/02/2016.
 */
public abstract class FlondrNavigationHandler extends AppCompatActivity implements NavigationView.OnNavigationItemSelectedListener {

    public static final String ConfigName = "MyPrefs";
    public static final String UserSession = "username";
    // Local Fields
    boolean backPressedOnce = false;
    SharedPreferences sharedPreferences;
    SharedPreferences.Editor configEditor;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public void onBackPressed() {
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawerLayout);
        // Close navigation menu if existed
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);

            // For debugging purpose
            Log.i("Navigation Menu", "From " + this.getLocalClassName() + ": Back pressed");

        } else {

            // Double back to exit code
            if (backPressedOnce) {
                super.onBackPressed();
                return;
            }
            this.backPressedOnce = true;
            Toast.makeText(FlondrNavigationHandler.this, "Press BACK again to exit.", Toast.LENGTH_SHORT).show();
            new Handler().postDelayed(new Runnable() {
                @Override
                public void run() {
                    backPressedOnce = false;
                }
            }, 1500);
        }

    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        if (getLocalClassName().equals("Flondr_OutletPage")) {
            getMenuInflater().inflate(R.menu.outlet_menu, menu);
        } else {
            getMenuInflater().inflate(R.menu.flondr__main, menu);
        }
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.actionSettings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    @SuppressWarnings("StatementWithEmptyBody")
    @Override
    public boolean onNavigationItemSelected(MenuItem item) {
        // Handle navigation view item clicks here.
        int id = item.getItemId();

        if (id == R.id.navReview) {
            Intent intent = new Intent(this, Flondr_ListReview.class);
            startActivity(intent);
        } else if (id == R.id.navUpload) {

        } else if (id == R.id.navPromo) {
            Intent intent = new Intent(this, Flondr_FindPromos.class);
            intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
            startActivity(intent);
        } else if (id == R.id.navNearby) {
            Intent intent = new Intent(this, Flondr_Nearby.class);
            intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
            startActivity(intent);
        } else if (id == R.id.navSearch) {
            Intent intent = new Intent(this, Flondr_SearchPlaces.class);
            intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
            startActivity(intent);
        } else if (id == R.id.navBookmark) {
            Intent intent = new Intent(this, Flondr_Bookmark.class);
            intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
            startActivity(intent);
        } else if (id == R.id.navSignMenu) {
            Intent intent;
            if (!SessionManager.session().anyActiveSession(this)) intent = new Intent(this, Flondr_Signup.class);
            else {
                SessionManager.session().clearSession(this);
                intent = new Intent(this, Flondr_Main.class);
            }
            intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
            startActivity(intent);
        }

        // For debugging purpose
        Log.i("Navigation Menu", "From " + this.getLocalClassName() + ": Selecting " + item.toString());

        item.setChecked(false);
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawerLayout);
        drawer.closeDrawer(GravityCompat.START);

        return true;
    }

    /* End of session Handler */

    public void mainSignup(View view) {
        Intent intent = new Intent(this, Flondr_Signup.class);
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_NEW_TASK);
        startActivity(intent);
    }

    public void bottomNearby(View view) {
        Intent intent = new Intent(this, Flondr_Nearby.class);
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
        startActivity(intent);

        // For debugging purpose
        Log.i("bottom Menu", "From " + this.getLocalClassName() + ": Nearby bottom clicked");
    }

    public void bottomSearch(View view) {
        Intent intent = new Intent(this, Flondr_ListReview.class);
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
        startActivity(intent);

        // For debugging purpose
        Log.i("bottom Menu", "From " + this.getLocalClassName() + ": Search bottom clicked");
    }

    public void bottomSetting(View view) {
        Intent intent = new Intent(this, Flondr_UserProfile.class);
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
        startActivity(intent);

        // For debugging purpose
        Log.i("bottom Menu", "From " + this.getLocalClassName() + ": Setting bottom clicked");
    }

    public void dummyOutletPage(View view) {
        Intent intent = new Intent(this, Flondr_OutletPage.class);
        startActivity(intent);

        // For debugging purpose
        Log.i("bottom Menu", "From " + this.getLocalClassName() + ": Outlet clicked");
    }

    public void dummyReview(View view) {
        Intent intent = new Intent(this, Flondr_Review.class);
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
        startActivity(intent);

        // For debugging purpose
        Log.i("bottom Menu", "From " + this.getLocalClassName() + ": Review clicked");
    }

    public void dummyUser(View view) {
        Intent intent = new Intent(this, Flondr_UserProfile.class);
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
        startActivity(intent);

        // For debugging purpose
        Log.i("bottom Menu", "From " + this.getLocalClassName() + ": User profile clicked");
    }

    public void startPopUpSearch(View v) {
        // get your outer relative layout
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawerLayout);

        // inflate content layout and add it to the relative layout as second child
        // add as second child, therefore pass index 1 (0,1,...)

        View popup = getLayoutInflater().inflate(R.layout.app_search_popup, null);
        drawer.addView(popup, 1);

        SearchView searchview = (SearchView) findViewById(R.id.searchViewBar);

        searchview.setFocusable(true);
        searchview.setFocusableInTouchMode(true);
        searchview.requestFocus();

        ((InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE)).
                toggleSoftInput(InputMethodManager.SHOW_FORCED,
                        InputMethodManager.HIDE_IMPLICIT_ONLY);

        searchview.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
            @Override
            public boolean onQueryTextChange(String newText) {
                TextView textBar = (TextView) findViewById(R.id.searchViewTextView);
                textBar.setText(newText);
                return true;
            }

            @Override
            public boolean onQueryTextSubmit(String query) {
                TextView textBar = (TextView) findViewById(R.id.searchViewTextView);
                textBar.setText(query);
                return true;
            }
        });
    }

    public void exitFromPopupSearch(View v) {

        // get your outer relative layout
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawerLayout);

        InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
        imm.hideSoftInputFromWindow(v.getWindowToken(), 0);

        drawer.removeView(drawer.getChildAt(1));
    }

    public void setUsernameForMenu(NavigationView navigationView){
        View view = navigationView.inflateHeaderView(R.layout.nav_header_flondr__main);
        TextView textView = (TextView) view.findViewById(R.id.textViewHeader);
        String s;

        s = "Welcome, " + SessionManager.session().getUserSession(this);

        Log.i("User", "Now logged in " + SessionManager.session().getUserSession(this));

        textView.setText(s);
    }

    public void setContentViewWrapper(int layoutID){
        /*
        Method ini digunakan untuk membungkus method setContentView yang mungkin terjadi konflik
        jika di override, sehingga semua fungsi yang sama di setiap activity akan dibungkus disini
         */
        setContentView(layoutID);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawerLayout);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.setDrawerListener(toggle);
        toggle.syncState();
        NavigationView navigationView = (NavigationView) findViewById(R.id.navView);
        navigationView.setNavigationItemSelectedListener(this);
        setUsernameForMenu(navigationView);
    }
}
